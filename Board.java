public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String flippedTiles = "";
		for(int i = 0; i < this.tiles.length; i++){
			if(!(this.tiles[i])){
				flippedTiles += i+1 + " ";
			} else {
				flippedTiles += "X ";
			}
		}
		return flippedTiles;
	}
	
	public boolean playATurn(){
		die1.roll();
		System.out.println("Die 1: " + die1);
		die2.roll();
		System.out.println("Die 2: " + die2);
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		if(!(this.tiles[sumOfDice - 1])){
			this.tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}else if(!(this.tiles[die1.getFaceValue() - 1])){
			this.tiles[die1.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die one: " + die1);
			return false;
		}else if(!(this.tiles[die2.getFaceValue() - 1])){
			this.tiles[die1.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die two: " + die2);
			return false;
		}else{
			System.out.println("All the tiles for these values are already shut.");
			return true;
		}
	}
}