import java.util.Random;

public class Die{
	Random rand = new Random();
	private int faceValue;
	
	public Die(){
		this.faceValue = 1;
	}
	
	public String toString(){
		return Integer.toString(this.faceValue);
	}

	public int getFaceValue(){
		return this.faceValue;
	}

	public void roll(){
		this.faceValue = rand.nextInt(6)+1;
	}
}