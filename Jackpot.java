import java.util.Scanner;

public class Jackpot{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Let's play jackpot!");
		Boolean endGame = false;
		int gamesWon = 0;
		
		while(!(endGame)){
			Board board = new Board();
			Boolean gameOver = false;
			int numOfTilesClosed = 0;
			
			while(!(gameOver)){
				System.out.println(board);
				if(board.playATurn()){
					gameOver = true;
				}else{
					numOfTilesClosed++;
				}
			}
			
			if(numOfTilesClosed >= 7){
				System.out.println("You reached the jackpot!");
				gamesWon++;
			}else{
				System.out.println("Too bad! You lost the jackpot.");
			}
			
			System.out.println("You won " + gamesWon + " game(s).");
			System.out.println("Would you like to continue playing? Enter [0] for NO, any other number for YES.");
			int userInput = reader.nextInt();
			
			if(userInput == 0){
				System.out.println("Thanks for playing!");
				endGame = true;
			}
		}
	}
}